import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class BrouserCountTest
{
    MapDriver<Object, Text, Text, IntWritable> mapDriver;
    BrouserCount.TokenizerMapper mapper;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    BrouserCount.IntSumReducer reducer;
    MapReduceDriver<Object, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp()
    {
        /* setup mapper */
        mapper = new BrouserCount.TokenizerMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        /* setup reducer */
        reducer = new BrouserCount.IntSumReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);

        /* setup map-reducer */
        mapReduceDriver = new MapReduceDriver<Object, Text, Text, IntWritable, Text, IntWritable>();
        mapReduceDriver.setMapper(mapper);
        mapReduceDriver.setReducer(reducer);
    }

    @Test
    public void testMapper(){
        String inputString = "188.192.63.123 - - [24/Oct/2017:23:52:25 +0300] \"PUT /posts/posts/explore HTTP/1.0\" 200 4969 \"http://king-rice.org/faq.asp\" \"Mozilla/5.0 (X11; Linux i686; rv:1.9.5.20) Gecko/2013-11-17 18:08:29 Firefox/14.0\"";
        mapDriver.withInput(new LongWritable(),new Text(inputString));
        mapDriver.withOutput(new Text("Firefox/14.0"), new IntWritable(1));

        mapDriver.runTest();
    }

    @Test
    public void testReducer(){
        ArrayList<IntWritable> array = new ArrayList<IntWritable>();
        array.add(new IntWritable(1));
        array.add(new IntWritable(1));
        reduceDriver.withInput(new Text("Firefox/3.8"), array);

        reduceDriver.withOutput(new Text("Firefox/3.8"), new IntWritable(2));

        reduceDriver.runTest();
    }

    @Test
    public void testMapReducer(){
        mapReduceDriver.withInput(new Object(), new Text("81.73.150.239 - - [10/Oct/2013:06:56:30 ]" +
                " \"GET /seatposts HTTP/1.0\" 101 3730 \"http://www.casualcyclist.com\" \"Mozilla/5.0" +
                " (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0\"\n" +
                "10.182.189.79 - - [10/Oct/2013:06:59:12 ] \"GET /saddles HTTP/1.0\" 102 2609 \"-\"" +
                " \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko)" +
                " Chrome/36.0.1944.0 Safari/537.36\"\n" +
                "16.180.70.237 - - [10/Oct/2013:07:00:38 ] \"GET /wheelsets HTTP/1.0\" 103 3246" +
                " \"http://bleater.com\" \"Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0\"\n"));
        mapReduceDriver.addOutput(new Text("Firefox/31.0"), new IntWritable(2));
        mapReduceDriver.addOutput(new Text("Safari/537.36"), new IntWritable(1));

        mapReduceDriver.runTest();
    }

}
